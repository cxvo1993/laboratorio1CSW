<?php 

//include_once "conexion.php";

include "claseConexion.php";

$db = new Conexion();

date_default_timezone_set('America/Guayaquil');

if (!empty($_POST)) {
	$idAdopcion = isset($_POST["idAdopcion"]) ? $_POST["idAdopcion"] : NULL;
	$idPerro = isset($_POST['idPerro']) ? $_POST['idPerro'] : NULL;
	$idCliente = isset($_POST['idCliente']) ? $_POST['idCliente'] : NULL;
	$fechaAdopcion = date('Y-m-d H:i:s');
	$observacionAdopcion = isset($_POST["observacionAdopcion"]) ? $_POST["observacionAdopcion"] : NULL;
}

if (isset($_POST["idAdopcion"]) || isset($_POST["idPerro"]) || isset($_POST["idCliente"]) || isset($_POST["observacionAdopcion"]) ) {
	if ($_POST["idAdopcion"] == "" || $_POST["idPerro"] == "" || $_POST["idCliente"] == "" || $_POST["observacionAdopcion"] == "") {
		print("<script>alert('Por favor no deje ningún campo vacío.')</script>");
	}else{

		$sql = "UPDATE adopciones SET perro=$idPerro, cliente='$idCliente', fechaAdopcion='$fechaAdopcion', observacion='$observacionAdopcion' where id=$idAdopcion";

		//$query=$con->query($sql);

		$query=$db->query($sql);

		if ($query != null) {
			
			header('Location: listadoAdopciones.php');
			print "<script>alert(\"Se registro la adopción satisfactoriamente satisfactoriamente.\");</script>";
		}else{
			
			header('Location: listadoAdopciones.php');
			print "<script>alert(\"Hubo un error, comuníquese con el administrador.\");</script>";
		}
	}
}else{
	print("<p>Algo ha andado mal</p>");
}
?>