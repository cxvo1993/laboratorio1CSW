<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Registro perro</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
</head>
<body>

	<div class="container">

		<h1 class="text-center">FORMULARIO EDICIÓN ADOPCIÓN DE PERRO</h1>

		<?php
		//include_once "conexion.php";

		include "claseConexion.php";

		$db = new Conexion();

		$sql = "select * from adopciones where id=".$_GET["id"];

		//$query = $con->query($sql);

		$query = $db->query($sql);
		$adopcion = null;

		if ($query -> num_rows > 0) {
			while ($r=$query -> fetch_object()){
				$adopcion=$r;
			}
		}

		$sqlPerro= "select * from perros where id=".$adopcion->perro;

		$queryPerro = $db->query($sqlPerro);
		$perro = null;

		if ($queryPerro -> num_rows > 0) {
			while ($r=$queryPerro -> fetch_object()){
				$perro=$r;
			}
		}

		$sqlCliente= "select * from clientes where id='".$adopcion->cliente."'";

		$queryCliente = $db->query($sqlCliente);
		$cliente = null;

		if ($queryCliente -> num_rows > 0) {
			while ($r=$queryCliente -> fetch_object()){
				$cliente=$r;
			}
		}



		?>

		<form action="editarAdopcion.php" method="post">

			<fieldset>
				<legend>Datos del perro</legend>
				<div class="form-row">
					<div class="form-group col-md-6">
						<label for="nombre">Nombre:</label>
						<input type="text" class="form-control" name="nombre" value="<?php echo $perro->nombre; ?>" placeholder="Escriba el nombre del perro" readonly>
					</div>
					<div class="form-group col-md-6">
						<label for="raza">Raza</label>
						<input type="text" class="form-control" name="raza" value="<?php echo $perro->raza; ?>" placeholder="Escriba la raza del perro" readonly>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-6">
						<label for="color">Color: </label>
						<input type="text" class="form-control" name="color" value="<?php echo $perro->color; ?>" placeholder="Escriba color perro" readonly>
					</div>
					<div class="form-group col-md-6">
						<label for="observacion">Observación: </label>
						<textarea name="observacion" class="form-control" placeholder="Ingrese alguna información adicional" readonly><?php echo $perro->observacion; ?></textarea>
					</div>
					<input type="hidden" name="idPerro" value="<?php echo $perro->id; ?>">
					<input type="hidden" name="idAdopcion" value="<?php echo $adopcion->id; ?>">
				</div>
			</fieldset>

			<fieldset>
				<legend>Datos del cliente</legend>
				<div class="form-row">
					<div class="form-group col-md-6">
						<div class="form-group">
							<label for="cliente">Clientes (seleccione un cliente de la lista):</label>
							<input list="clientes" class="form-control" name="lClientes" id="lClientes">
							<datalist id="clientes">

							</datalist>
						</div>
						<div class="form-group">
							<button type="button" id="cargarCliente" class="btn btn-info">Cargar Cliente</button>
						</div>
					</div>
					<div class="form-group col-md-6">
						<div class="form-group">
							<label for="id">Id (cedula):</label>
							<input type="text" class="form-control" name="idCliente" id="idCliente" value="<?php echo $cliente->id ?>" placeholder="Escoja un cliente" readonly>
						</div>
						<div class="form-group">
							<label for="apellidoNombre">Apellidos y Nombre:</label>
							<input type="text" class="form-control" name="apellidoNombreCliente" id="apellidoNombreCliente" value="<?php echo $cliente->apellidoNombre ?>" placeholder="Escoja un cliente" readonly>
						</div>
						<div class="form-group">
							<label for="email">Email: </label>
							<input type="email" class="form-control" name="emailCliente" id="emailCliente" value="<?php echo $cliente->email ?>" placeholder="Escoja un cliente" readonly>
						</div>
					</div>
				</div>
			</fieldset>

			<fieldset>
				<legend>Adopción</legend>
				<div class="form-row">
					<div class="form-group col-md-6">
						<div class="form-group">
							<label for="fechaAdopcion">Fecha Adopción: </label>
							<input type="datetime" name="fechaAdopcion" class="form-control" value="<?php echo $adopcion->fechaAdopcion ?>" required>
						</div>
					</div>
					<div class="form-group col-md-6">
						<div class="form-group">
							<label for="observacion">Observación: </label>
							<textarea name="observacionAdopcion" class="form-control" placeholder="Ingrese alguna información adicional" required><?php echo $adopcion->observacion ?></textarea>
						</div>
					</div>
				</div>
			</fieldset>

			<br />

			<div class="form-group">
				<input type="submit" class="btn btn-success" name="registrar" value="Actualizar Adopción">
				<a class="btn btn-info" href="listadoAdopciones.php" role="button">Cancelar y volver</a>
			</div>
		</form>
	</div>

	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>

	<script type="text/javascript">
		$(document).ready(function() {
			$.ajax({
				data:{},
				url:'traerClientes.php',
				success:function (response) {
					$("#clientes").html(response);
				}
			});
		});

		$("#cargarCliente").on("click", function(){
			var cliente = $("#lClientes").val();
			$.ajax({
				data:{"id" : cliente},
				url:'traerCliente.php',
				success:function (response) {
					var id=response.split("-")[0];
					var apellidosNombres=response.split("-")[1];
					var email=response.split("-")[2];
					$("#idCliente").val(id);
					$("#apellidoNombreCliente").val(apellidosNombres);
					$("#emailCliente").val(email);
				}
			});
		});
		
	</script>
</body>
</html>