<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Editar cliente</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
</head>
<body>

	<?php 
	//include_once "conexion.php";

	include "claseConexion.php";

	$db = new Conexion();

	$sql = "select * from clientes where id = '".$_GET["id"]."'";
	//$query = $con -> query($sql);
	$query = $db -> query($sql);
	$cliente = null;

	if ($query -> num_rows > 0) {
		while ( $r=$query->fetch_object() ) {
			$cliente=$r;
		}
	}
	?>
	
	<div class="container">
		<h1 class="text-center">FORMULARIO EDITAR CLIENTE</h1>

		<form action="editarCliente.php" method="post">
			<div class="form-group">
				<label for="id">Id (cedula):</label>
				<input type="text" class="form-control" name="id" value="<?php echo $cliente->id; ?>" placeholder="Escriba el número de cédula del cliente" required>
			</div>
			<div class="form-group">
				<label for="apellidoNombre">Apellidos y Nombre:</label>
				<input type="text" class="form-control" name="apellidoNombre" value="<?php echo $cliente->apellidoNombre; ?>" placeholder="Escriba apellidos y nombre del cliente" required>
			</div>
			<div class="form-group">
				<label for="fechaRegistro">Fecha Registro</label>
				<input type="text" class="form-control" name="fechaRegistro" value="<?php echo  $cliente->fechaRegistro; ?>" placeholder="Escriba la fecha del registro" required>
			</div>
			<div class="form-group">
				<label for="email">Email: </label>
				<input type="email" class="form-control" name="email" value="<?php echo  $cliente->email; ?>" placeholder="Escriba el email del cliente" required>
			</div>
			<div class="form-group">
				<label for="telefono">Telefono: </label>
				<input type="text" class="form-control" name="telefono" value="<?php echo  $cliente -> telefono; ?>" placeholder="Escriba el número de telefono" required>
			</div>
			<div class="form-group">
				<label for="direccion">Dirección: </label>
				<textarea name="direccion" class="form-control" required><?php echo $cliente -> dirección; ?></textarea>
			</div>
			<div class="form-group">
				<input type="hidden" name="id" value="<?php echo $cliente -> id; ?>">
				<input type="submit" class="btn btn-success" name="editar" value="Editar">
				<a class="btn btn-info" href="listadoClientes.php" role="button">Cancelar y volver</a>
			</form>
		</div>

		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
	</body>
	</html>