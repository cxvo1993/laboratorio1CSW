<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Registro cliente</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
</head>
<body>

	<div class="container">

		<h1 class="text-center">FORMULARIO REGISTRO CLIENTE</h1>

		<form action="registrarCliente.php" method="post">
			<div class="form-group">
				<label for="id">Id (Cédula)</label>
				<input type="text" class="form-control" name="id" value="" placeholder="Escriba el número de cedula del cliente" required>
			</div>
			<div class="form-group">
				<label for="apellidoNombre">Apellidos y Nombre</label>
				<input type="text" class="form-control" name="apellidoNombre" value="" placeholder="Escriba apellidos y nombres del cliente" required>
			</div>
			<div class="form-group">
				<label for="email">Email: </label>
				<input type="email" class="form-control" name="email" value="" placeholder="Escriba el correo electrónico" required>
			</div>
			<div class="form-group">
				<label for="telefono">Teléfono: </label>
				<input type="telefono" class="form-control" name="telefono" value="" placeholder="Escriba el número de telefono" required>
			</div>
			<div class="form-group">
				<label for="direccion">Dirección</label>
				<textarea name="direccion" class="form-control" placeholder="Ingrese la dirección del nuevo cliente" required></textarea>
			</div>
			<div class="form-group">
				<input type="submit" class="btn btn-success" name="registrar" value="Registrar">
				<a class="btn btn-info" href="listadoClientes.php" role="button">Cancelar y volver</a>
			</form>
		</div>

		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
	</body>
	</html>