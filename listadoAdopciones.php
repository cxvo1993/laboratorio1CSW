<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Listado Adopciones</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
</head>
<body>

	<div class="container">
		<h1 class="text-center">ADOPCIÓN CANINA</h1>
		<p class="text-center">Laboratorio 1 - Computación en el Servidor Web - UNIR</p>
		<br />

		<div class="text-center">
			<a href="index.php" title="">Listado de perros</a>
			<span> | </span>
			<a href="listadoClientes.php" title="">Listado de clientes</a>
		</div>

		<br/>

		<u><h2 class="text-center">Listado de adopciones</h2></u>

		<?php 

		//include_once "conexion.php";

		include "claseConexion.php";

		$db = new Conexion();

		$sql1= "SELECT a.id as id, CONCAT('(',p.id,') ', p.nombre) as perro, CONCAT('(',c.id,') ', c.apellidoNombre) as cliente, c.email as email, a.fechaAdopcion as fechaAdopcion, a.observacion as observacion FROM `perros` p, `clientes` c, `adopciones` a WHERE a.perro=p.id and a.cliente=c.id";
		//$query = $con->query($sql1);

		$query = $db->query($sql1);

		?>

		<div class="text-right">
			<h5>Número de adopciones registrados: <?php echo "".isset($query -> num_rows) ? $query -> num_rows : '0'.""; ?></h5>
		</div>
		<table class="table table-hover table-responsive">
			<thead class="thead-light">
				<tr>
					<th class="text-center">id</th>
					<th class="text-center">Perro</th>
					<th class="text-center">Cliente</th>
					<th class="text-center">Email Cliente</th>
					<th class="text-center">Fecha Adopción</th>
					<th class="text-center">Observacion</th>
					<th colspan="2" class="text-center">Acciones</th>
				</tr>
			</thead>
			<tbody>
				<?php if ($query -> num_rows > 0) { while ($r=$query->fetch_array()):  ?>

					<tr>
						<td><?php echo "".$r["id"].""; ?></td>
						<td><?php echo "".$r["perro"].""; ?></td>
						<td><?php echo "".$r["cliente"].""; ?></td>
						<td><?php echo "".$r["email"].""; ?></td>
						<td><?php echo "".$r["fechaAdopcion"]."" ?></td>
						<td><?php echo "".$r["observacion"].""; ?></td>
						<td><a href='formularioEditarAdopcion.php?id=<?php echo $r["id"] ?>' class="btn btn-success" role="button" title=''>Editar</a></td>
						<td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modalEliminarAdopcion" onclick="enviarIdAdopcion('<?php echo "".$r["id"]."" ?>');">
							Eliminar
						</button></td>
					</tr>

				<?php endwhile ; } else { ?>	

				<tr><td colspan="8">No existe registro</td></tr>

				<?php ; } ?>

			</tbody>
		</table>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="modalEliminarAdopcion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Eliminar registro adopción</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					¿Está seguro de eliminar la adopción seleccionada?
				</div>
				<div class="modal-footer">

					<form action="eliminarAdopcion.php" method="post">
						<input type="hidden" id="oculto" name="oculto" value="">
						<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
						<input type="submit" class="btn btn-danger" value="Si, eliminar">
					</form>

				</div>
			</div>
		</div>
	</div>

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

	<script type="text/javascript">
		function enviarIdAdopcion(id){
			$("#oculto").val(id);
		}
	</script>

</body>
</html>