<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Editar perro</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
</head>
<body>

	<?php 
	//include_once "conexion.php";

	include "claseConexion.php";

	$db = new Conexion();

	$sql = "select * from perros where id = ".$_GET["id"];
	//$query = $con -> query($sql);
	$query = $db -> query($sql);
	$perro = null;

	if ($query -> num_rows > 0) {
		while ( $r=$query->fetch_object() ) {
			$perro=$r;
		}
	}
	?>
	
	<div class="container">

		<h1 class="text-center">FORMULARIO EDITAR PERRO</h1>
		<form action="editarPerro.php" method="post">
			<div class="form-group">
				<label for="nombre">Nombre:</label>
				<input type="text" class="form-control" name="nombre" value="<?php echo $perro->nombre; ?>" placeholder="Escriba el nombre del perro" required>
			</div>
			<div class="form-group">
				<label for="raza">Raza</label>
				<input type="text" class="form-control" name="raza" value="<?php echo  $perro->raza; ?>" placeholder="Escriba la raza del perro" required>
			</div>
			<div class="form-group">
				<label for="fechaRegistro">Fecha Registro: </label>
				<input type="datetime" class="form-control" name="fechaRegistro" value="<?php echo  $perro->fechaRegistro; ?>" required>
			</div>
			<div class="form-group">
				<label for="color">Color: </label>
				<input type="text" name="color" class="form-control" value="<?php echo  $perro -> color; ?>" placeholder="Escriba color perro" required>
			</div>
			<div class="form-group">
				<label for="observacion">Observación: </label>
				<textarea name="observacion" class="form-control" placeholder="Ingrese una información adicional del perro" required><?php echo $perro -> observacion; ?></textarea>
			</div>
			<div class="form-group">
				<input type="hidden" name="id" value="<?php echo $perro -> id; ?>">
				<input type="submit" class="btn btn-success" name="editar" value="Actualizar">
				<a class="btn btn-info" href="index.php" role="button">Cancelar y volver</a>
			</div>
		</form>
	</div>

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</body>
</html>