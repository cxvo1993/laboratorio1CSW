<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Adopción Canina</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
</head>
<body>
	<div class="container">
		<h1 class="text-center">ADOPCIÓN CANINA</h1>
		<p class="text-center">Laboratorio 1 - Computación en el Servidor Web - UNIR</p>
		<br />

		<div class="text-center">
			<a href="listadoClientes.php" title="">Listado Clientes</a>
			<span> | </span>
			<a href="listadoAdopciones.php" title="">Listado de adopciones</a>
		</div>

		<br />

		<u><h2 class="text-center">Listado de perros para adopción</h2></u>
		<div class="text-center">
			<a href="formularioRegistroPerro.html" class="btn btn-primary" rol="button" title="">Registrar nuevo perro</a>
		</div>

		<?php 

		//include_once "conexion.php";

		include "claseConexion.php";

		$db = new Conexion();

		$sql1= "Select p.* from `perros` p where not exists (select 1 from `adopciones` a where a.perro = p.id)";
		//$query = $con->query($sql1);
		$query = $db->query($sql1);

		?>

		<div class="text-right">
			<h5>Perros disponibles para adopción: <?php echo "".isset($query -> num_rows) ? $query -> num_rows : '0'.""; ?></h5>
		</div>
		<table class="table table-hover table-responsive">

			<thead class="thead-light">
				<tr>
					<th class="text-center" width="5%">id</th>
					<th class="text-center" width="10%">Nombre</th>
					<th class="text-center" width="10%">Raza</th>
					<th class="text-center" width="15%">Fecha Registro</th>
					<th class="text-center">Color</th>
					<th class="text-center" width="15%">Observacion</th>
					<th class="text-center" colspan="3" width="15%">Acciones</th>
				</tr>
			</thead>
			<tbody>
				<?php if ($query -> num_rows > 0) { while ($r=$query->fetch_array()):  ?>

					<tr>
						<td><?php echo "".$r["id"].""; ?></td>
						<td><?php echo "".$r["nombre"].""; ?></td>
						<td><?php echo "".$r["raza"].""; ?></td>
						<td><?php echo "".$r["fechaRegistro"].""; ?></td>
						<td><?php echo "".$r["color"]."" ?></td>
						<td><?php echo "".$r["observacion"].""; ?></td>
						<td><a href='formularioAdoptarPerro.php?id=<?php echo $r["id"] ?>' title='' class="btn btn-primary">Adopción</a></td>
						<td><a href='formularioEditarPerro.php?id=<?php echo $r["id"] ?>' title='' class="btn btn-success">Editar</a></td>
						<td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modalEliminarPerro" onclick="enviarIdPerro('<?php echo "".$r["id"]."" ?>', '<?php echo "".$r["nombre"].""; ?>', '<?php echo "".$r["raza"].""; ?>');">
							Eliminar
						</button></td>
					</tr>

				<?php endwhile ; } else { ?>	

				<tr><td colspan="9">No existe registro</td></tr>

				<?php ; } ?>	

			</tbody>
		</table>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="modalEliminarPerro" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Eliminar perro</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					¿Está seguro de eliminar al perro con id <span id="idPerro"></span>, de nombre <span id="nombrePerro"></span> y de raza <span id="razaPerro"></span> ?
				</div>
				<div class="modal-footer">

					<form action="eliminarPerro.php" method="post">
						<input type="hidden" id="oculto" name="oculto" value="">
						<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
						<input type="submit" class="btn btn-danger" value="Si, eliminar">
					</form>
					
				</div>
			</div>
		</div>
	</div>

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

	<script type="text/javascript">
		function enviarIdPerro(id, nombre, raza){
			$("#idPerro").html(id);
			$("#nombrePerro").html(nombre);
			$("#razaPerro").html(raza);

			$("#oculto").val(id);
		}
	</script>
</body>
</html>